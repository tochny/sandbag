import sys
import json



def main(args):
    print('Get redis endpoint')
    endpoint = sys.argv[1]
    print(endpoint)
    with open("redis.endpoint", "w+") as data_file:
        data_file.write(endpoint)

if __name__ == '__main__':
    main(sys.argv)
